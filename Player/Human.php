<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:14
 */

namespace Player;


use Helpers\Combination;

class Human extends PlayerAbstract
{

    /**
     * @param $propositionHistory
     *
     * @return int
     */
    public function getProposition($propositionHistory)
    {
        parent::getProposition($propositionHistory);
        do {
            $proposition = $this->getRenderer()->getInput($this->getName() . ', quelle est votre proposition ?');
            try {
                Combination::getHelper()->checkCombination($proposition);
            } catch (\RuntimeException $e) {
                $proposition = -1;
                $this->getRenderer()->addError('Proposition invalide: '.$e->getMessage());
            }
        } while ($proposition == -1);

        return $proposition;
    }

    /**
     * @return $this
     */
    public function getSolutionToGuess()
    {
        $this->getRenderer()->clear();
        do {
            $proposition = $this->getRenderer()->getInput($this->getName() . ', quelle est la solution à trouver ?');
            try {
                Combination::getHelper()->checkCombination($proposition);
            } catch (\RuntimeException $e) {
                $proposition = -1;
                $this->getRenderer()->addError('Proposition invalide: '.$e->getMessage());
            }
        } while ($proposition == -1);

        return $proposition;
    }

} 