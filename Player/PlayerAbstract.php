<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Player;


use Renderer\RendererAbstract;

abstract class PlayerAbstract {
    /**
     * @var string $_name
     */
    protected $_name = null;
    /**
     * @var RendererAbstract $_renderer
     */
    protected $_renderer = null;

    /**
     * @param $propositionHistory
     *
     * @return int
     */
    public function getProposition($propositionHistory){
        $this->getRenderer()
            ->clear()
            ->displayPropositionsHistory($propositionHistory);

        return 0;
    }

    /**
     * @return $this
     */
    abstract public function getSolutionToGuess();

    public function __construct($name = null, $renderer = null){
        if(!is_null($name)){
            $this->setName($name);
        }
        if(!is_null($renderer)){
            $this->setRenderer($renderer);
        }
    }

    /**
     * @param RendererAbstract $renderer
     *
     * @return $this
     */
    public function setRenderer(RendererAbstract $renderer){
        $this->_renderer = $renderer;
        return $this;
    }

    /**
     * @return RendererAbstract
     */
    public function getRenderer(){
        return $this->_renderer;
    }


    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name){
        $this->_name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->_name;
    }
} 