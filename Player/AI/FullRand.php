<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Player\AI;


use Helpers\Combination;

class FullRand extends AIBase1 {

    /**
     * @param $propositionHistory
     *
     * @return int
     */
    public function getProposition($propositionHistory){
        parent::getProposition($propositionHistory);
        $proposition = $this->_getRandomProposition();
        do{
            if($this->_isValidSolution($propositionHistory, $proposition)){
                return $proposition;
            }

            $proposition = $this->_getRandomProposition();
        }while(true);
    }
}