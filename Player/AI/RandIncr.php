<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Player\AI;


use Helpers\Combination;

class RandIncr extends AIBase1 {
    /**
     * @param $propositionHistory
     *
     * @return int
     */
    public function getProposition($propositionHistory){
        parent::getProposition($propositionHistory);
        $proposition = $this->_getRandomProposition();
        do{
            if($this->_isValidSolution($propositionHistory, $proposition)){
                return $proposition;
            }

            $proposition++;
            try{
                Combination::getHelper()->checkCombination($proposition);
            }catch(\RuntimeException $e){
                $proposition = "0000";
            }
        }while(true);
    }
} 