<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Player\AI;


use Helpers\Combination;
use Player\PlayerAbstract;

abstract class AIBase1 extends PlayerAbstract {

    /**
     * @return $this
     */
    public function getSolutionToGuess(){
        return $this->_getRandomProposition();
    }

    protected function _getRandomProposition(){
        return rand(0,pow(10,Combination::NB_ELEMENTS)-1);
    }

    /**
     * @param $propositionHistory
     * @param $proposition
     *
     * @return bool
     */
    protected function _isValidSolution($propositionHistory, $proposition)
    {
        $validSolution = true;
        foreach ($propositionHistory as $history) {
            $compare = Combination::getHelper()->compareCombinations($history['proposition'], $proposition);

            if ($proposition == $history['proposition']
                || ($compare['good'] != $history['good'] && $compare['wrong'] != $history['wrong'])
            ) {
                $validSolution = false;
                break;
            }
        }
        return $validSolution;
    }

}