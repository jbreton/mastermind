<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Player\AI;


use Helpers\Combination;

class FullIncr extends AIBase1 {
    /**
     * @param $propositionHistory
     *
     * @return int
     */
    public function getProposition($propositionHistory){
        parent::getProposition($propositionHistory);
        $proposition = "0000";
        do{
            if($this->_isValidSolution($propositionHistory, $proposition)){
                return $proposition;
            }

            $proposition++;
            Combination::getHelper()->checkCombination($proposition);
        }while(true);
    }
} 