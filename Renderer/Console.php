<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:13
 */

namespace Renderer;


class Console extends RendererAbstract
{

    public function clear()
    {
        print chr(27) . "[2J" . chr(27) . "[;H";

        return $this;
    }

    public function getInput($question)
    {
        echo "\n", $question, "\n";
        $handle = fopen("php://stdin", "r");
        $line = trim(fgets($handle));

        return $line;
    }

    public function displayMenu($menu)
    {
        $i = 1;
        $line = 0;
        $intFunctions = array();
        foreach ($menu as $question => $function) {
            echo $i, '. ', $question, "\n";
            $intFunctions['rep' . $i] = $function;
        }
        while (array_key_exists('rep' . $line, $intFunctions)) {
            $handle = fopen("php://stdin", "r");
            $line = trim(fgets($handle));
        }

        return call_user_func($intFunctions['rep' . $line]);
    }

    function displayPropositionsHistory($propositions)
    {
        $good = chr(27) . '[1;32mX' . chr(27) . '[0m';
        $wrong = chr(27) . '[1;33m?' . chr(27) . '[0m';
        $absent = chr(27) . '[1;30m.' . chr(27) . '[0m';

        echo $good, ' : Good, ', $wrong, ' : Wrong place, ', $absent, ' : Bad',"\n\n";
        foreach ($propositions as $i => $proposition) {
            echo $i, ' : ', "\t", $proposition['proposition'], "\t";
            echo str_repeat($good, $proposition['good']);
            echo str_repeat($wrong, $proposition['wrong']);
            echo str_repeat($absent, $proposition['absent']);
            echo "\n";
        }
        echo "\n\n";
        return $this;
    }

    /**
     * Display an error message
     *
     * @param $msg
     * @return $this
     */
    function addError($msg){
        echo "\n",chr(27),'[1;31m',$msg,chr(27),'[0m',"\n";
        return $this;
    }

    /**
     * Display a success message
     *
     * @param $msg
     * @return $this
     */
    function addSuccess($msg){
        echo "\n",chr(27),'[1;32m',$msg,chr(27),'[0m',"\n";
        return $this;
    }
} 