<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:12
 */

namespace Renderer;


abstract class RendererAbstract {

    /**
     * Clears display
     *
     * @return $this
     */
    abstract function clear();

    /**
     * Asks a question to the human
     *
     * @param $question String
     *
     * @return string
     */
    abstract function getInput($question);

    /**
     * Display a menu to the human and executes bundled question
     * @param $menu array('proposition', callable function with call_user_func());
     *
     * @return mixed call_user_func() result
     */
    abstract function displayMenu($menu);

    /**
     * Display propositions history
     * @param $propositions
     *
     * @return $this
     */
    abstract function displayPropositionsHistory($propositions);

    /**
     * Display an error message
     *
     * @param $msg
     * @return $this
     */
    abstract function addError($msg);

    /**
     * Display a success message
     *
     * @param $msg
     * @return $this
     */
    abstract function addSuccess($msg);
} 