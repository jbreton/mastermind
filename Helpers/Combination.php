<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:29
 */

namespace Helpers;


class Combination
{

    /**
     * Number of elements in the solution
     */
    const NB_ELEMENTS = 4;

    /**
     * Current combination singleton instance
     * @var Combination $_self
     */
    static protected $_self = null;

    /**
     * Checks combination number validity
     *
     * @param $combination
     *
     * @throws \OutOfBoundsException
     */
    public function checkCombination(&$combination)
    {
        if (!is_numeric($combination)) {
            throw new \UnexpectedValueException('This combination is not number');
        }
        if ($combination != round($combination)) {
            throw new \UnexpectedValueException('This combination is not an integer');
        }
        if ($combination >= pow(10, self::NB_ELEMENTS)) {
            throw new \OutOfBoundsException('This combination have too much elements');
        }
        if ($combination < 0) {
            throw new \RangeException('This combination is a negative number');
        }
        $this->cleanCombination($combination);
    }

    public function cleanCombination(&$combination){
        $combination = abs(round($combination));
        $combination = str_pad($combination,self::NB_ELEMENTS,'0', STR_PAD_LEFT);
        return $combination;
    }

    public function compareCombinations($toFind, $toTry)
    {
        $goodPlace = 0;
        $wrongPlace = 0;

        $toFind = str_split($toFind);
        $toTry = str_split($toTry);

        //Find good places
        for($i = 0;$i < self::NB_ELEMENTS; $i++){
            if ($toTry[$i] == $toFind[$i]) {
                $goodPlace++;
                unset($toFind[$i]);
                unset($toTry[$i]);
            }
        }

        //Find wrong places
        for ($i = 0; $i < self::NB_ELEMENTS; $i++) {
            for ($j = 0; $j < self::NB_ELEMENTS; $j++) {
                if (!is_null($toTry[$i]) && !is_null($toFind[$j]) && $toTry[$i] === $toFind[$j]) {
                    $wrongPlace++;
                    continue 2;
                }
            }
        }

        return array(
            'good' => $goodPlace,
            'wrong' => $wrongPlace,
            'absent' => self::NB_ELEMENTS - $goodPlace - $wrongPlace
        );
    }

    /**
     * Returns an helper instance
     * @return Combination
     */
    static public function getHelper()
    {
        if (is_null(self::$_self)) {
            self::$_self = new Combination();
        }
        return self::$_self;
    }
} 