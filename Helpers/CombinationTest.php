<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:56
 */

namespace Helpers;

class CombinationTest extends \PHPUnit_Framework_TestCase
{

    public function testCompareCombinations()
    {
        $tests = array(
            array(
                'find' => '0000',
                'try' => '0001',
                'wrong' => 0,
                'good' => 3,
                'absent' => 1,
            ),
            array(
                'find' => '1234',
                'try' => '5678',
                'wrong' => 0,
                'good' => 0,
                'absent' => 4,
            ),
            array(
                'find' => '2345',
                'try' => '1234',
                'wrong' => 3,
                'good' => 0,
                'absent' => 1,
            ),
            array(
                'find' => '3456',
                'try' => '3546',
                'wrong' => 2,
                'good' => 2,
                'absent' => 0,
            ),
            array(
                'find' => '1000',
                'try' => '0123',
                'wrong' => 2,
                'good' => 0,
                'absent' => 2,
            ),
            array(
                'find' => '1111',
                'try' => '4321',
                'wrong' => 0,
                'good' => 1,
                'absent' => 3,
            ),
            array(
                'find' => '2222',
                'try' => '3222',
                'wrong' => 0,
                'good' => 3,
                'absent' => 1,
            ),
            array(
                'find' => '1234',
                'try' => '1111',
                'wrong' => 0,
                'good' => 1,
                'absent' => 3,
            ),
            array(
                'find' => '4321',
                'try' => '1111',
                'wrong' => 0,
                'good' => 1,
                'absent' => 3,
            ),
        );

        foreach ($tests as $test) {
            $r = array(
                    'find' => $test['find'],
                    'try' => $test['try']
                ) + Combination::getHelper()->compareCombinations($test['find'], $test['try']);
            $this->assertEquals($test, $r);
        }
    }
}
 