<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 06:30
 */

namespace Helpers;


class GameTest extends \PHPUnit_Framework_TestCase {

    public function testGame(){
        $game = new Game();

        $game->setSolution('1234');

        $propositions = array(
            array(
                'proposition' => '1111',
                'good' => 1,
                'wrong' => 0,
                'absent' => 3
            ),
            array(
                'proposition' => '1222',
                'good' => 2,
                'wrong' => 0,
                'absent' => 2
            ),
            array(
                'proposition' => '1233',
                'good' => 3,
                'wrong' => 0,
                'absent' => 1
            ),
            array(
                'proposition' => '1234',
                'good' => 4,
                'wrong' => 0,
                'absent' => 0
            )
        );

        foreach($propositions as $p){
            $game->propose($p['proposition']);
        }

        $propositionsHistory = $game->getPropositions();

        $this->assertEquals($propositions, $propositionsHistory);
    }
}
 