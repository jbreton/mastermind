<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:12
 */

namespace Helpers;

use Player\PlayerAbstract;
use Renderer\RendererAbstract;

class Game
{
    /**
     * Solution to find
     * @var int $_solution
     */
    protected $_solution = null;

    /**
     * Guesses
     * @var array $_proposition
     */
    protected $_propositions = array();

    /**
     * Player
     * @var PlayerAbstract $_player1
     */
    protected $_player1 = null;

    /**
     * Player
     * @var PlayerAbstract $_player2
     */
    protected $_player2 = null;

    /**
     * Renderer
     * @var RendererAbstract $_renderer
     */
    protected $_renderer = null;

    /**
     * Reset current game
     */
    public function reset()
    {
        unset($this->_solution);
        $this->_propositions = array();
    }

    /**
     * Sets the combination to find
     *
     * @param $solution
     */
    public function setSolution($solution)
    {
        Combination::getHelper()->checkCombination($solution);
        $this->_solution = $solution;
    }

    /**
     * @param int $proposition
     *
     * @return array
     */
    public function propose($proposition)
    {
        Combination::getHelper()->checkCombination($proposition);
        $result = Combination::getHelper()->compareCombinations($this->_solution, $proposition);

        $this->_addProposition($proposition, $result);

        return $result;
    }

    /**
     * Adds proposition to array
     *
     * @param $proposition
     * @param $result
     *
     * @return $this
     */
    protected function _addProposition($proposition, $result){
        $this->_propositions[] = array('proposition' => $proposition) + $result;

        return $this;
    }

    /**
     * Get propositions
     */
    public function getPropositions(){
        return $this->_propositions;
    }

    public function run(){

        $solution = $this->_player1->getSolutionToGuess();

        $this->setSolution($solution);

        do{
            $proposition = $this->_player2->getProposition($this->getPropositions());

            $result = $this->propose($proposition);

        }while($result['good'] != Combination::NB_ELEMENTS);

        $this->getRenderer()->clear()->displayPropositionsHistory($this->getPropositions());

        $this->getRenderer()->addSuccess("C'est gagné, vous êtes un winner !");
    }

    /**
     * @return \Player\PlayerAbstract
     */
    public function getPlayer1()
    {
        return $this->_player1;
    }

    /**
     * @param \Player\PlayerAbstract $player1
     */
    public function setPlayer1($player1)
    {
        $player1->setRenderer($this->getRenderer());
        $this->_player1 = $player1;
    }

    /**
     * @return \Player\PlayerAbstract
     */
    public function getPlayer2()
    {
        return $this->_player2;
    }

    /**
     * @param \Player\PlayerAbstract $player2
     */
    public function setPlayer2($player2)
    {
        $player2->setRenderer($this->getRenderer());
        $this->_player2 = $player2;
    }

    /**
     * @return \Renderer\RendererAbstract
     */
    public function getRenderer()
    {
        return $this->_renderer;
    }

    /**
     * @param \Renderer\RendererAbstract $renderer
     */
    public function setRenderer($renderer)
    {
        $this->_renderer = $renderer;
    }
} 