<?php
/**
 * Created by PhpStorm.
 * User: Jerome
 * Date: 05/03/14
 * Time: 05:12
 */

require_once 'bootstrap.php';

$game = new Helpers\Game();

$game->setRenderer(new Renderer\Console());
$game->setPlayer1(new Player\Human('Joueur 1'));
$game->setPlayer2(new Player\AI\FullRand('Joueur 2'));

$game->run();